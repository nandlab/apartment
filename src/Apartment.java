import java.util.ArrayList;
import java.util.Scanner;

/**
 * An apartment, where you can simulate a tour.
 */
public class Apartment {
    private final ArrayList<Room> rooms;

    /**
     * Construct a new apartment.
     * @param entrance the entrance of the apartment
     */
    public Apartment(Room entrance) {
        rooms = new ArrayList<>();
        rooms.add(entrance);
    }

    /**
     * Add a room.
     * @param newRoom the new room
     */
    public void extension(Room newRoom) {
        rooms.add(newRoom);
    }

    /**
     * Simulate entering and touring the apartment.
     * @param scanner scanner for user input
     */
    public void enter(Scanner scanner) {
        rooms.get(0).enter(scanner);
        System.out.println();
        for (Room room : rooms) {
            if (room.getLight()) {
                System.out.println("Warning: " + room.getName() + ": Light still on.");
            }
            if (room instanceof Kitchen kitchen && kitchen.getStove()) {
                System.out.println("Warning: " + room.getName() + ": Stove still on.");
            }
            if (room instanceof Bathroom bathroom && bathroom.getShower()) {
                System.out.println("Warning: " + room.getName() + ": Shower still on.");
            }
        }
        System.out.println("Leaving apartment.");
    }
}
