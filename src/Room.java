import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Room {
    protected String name;
    private boolean lighting;
    private final ArrayList<Room> neighbours;

    public Room(String name, List<Room> neighbours) {
        this.name = name;
        this.lighting = false;
        if (neighbours == null) {
            this.neighbours = new ArrayList<>();
        }
        else {
            this.neighbours = new ArrayList<>(neighbours);
            for (Room neighbour : neighbours) {
                neighbour.neighbours.add(this);
            }
        }
    }

    public String getName() {
        return name;
    }

    public ArrayList<Room> getNeighbours() {
        return neighbours;
    }

    public boolean getLight() {
        return lighting;
    }

    public void setLight(boolean light) {
        lighting = light;
    }

    /**
     * Show a dialog for entering a neighbouring room.
     * @param scanner scanner for user input
     * @return zero if success, none-zero if failure
     */
    private int enterNeighbour(Scanner scanner) {
        System.out.println("Which room?");
        ArrayList<Room> neighbours = this.getNeighbours();
        for (int i = 0; i < neighbours.size(); i++) {
            System.out.println((i+1) + ") " + neighbours.get(i).getName());
        }
        if (scanner.hasNextInt()) {
            int n = scanner.nextInt() - 1;
            if (n >= 0 && n < neighbours.size()) {
                neighbours.get(n).enter(scanner);
            }
            else {
                System.out.println("Out of range!");
                return 1;
            }
        }
        else {
            scanner.next();
            System.out.println("Not an integer!");
            return 1;
        }
        return 0;
    }

    /**
     * Simulate entering this room.
     * @param scanner scanner for user input
     */
    public void enter(Scanner scanner) {
        boolean leave = false;
        do {
            System.out.println(System.lineSeparator() + "You are in the " + this.getName() + ".");
            System.out.println("Light: " + (this.getLight() ? "On" : "Off"));
            if (this instanceof Kitchen) { System.out.println("Stove: " + (((Kitchen) this).getStove() ? "On" : "Off")); }
            if (this instanceof Bathroom) { System.out.println("Shower: " + (((Bathroom) this).getShower() ? "On" : "Off")); }
            System.out.println("1) Enter neighbouring room");
            System.out.println("2) Leave room");
            System.out.println("3) Switch light");
            if (this instanceof Kitchen) { System.out.println("4) Switch stove"); }
            if (this instanceof Bathroom) { System.out.println("4) Switch shower"); }
            if (scanner.hasNextInt()) {
                int n = scanner.nextInt();
                switch (n) {
                    case 1 -> {
                        if (this.enterNeighbour(scanner) == 0) {
                            System.out.println("Returning to previous room.");
                        }
                        else {
                            System.out.println("Staying in the " + this.getName() +  ".");
                        }
                    }
                    case 2 -> leave = true;
                    case 3 -> {
                        this.setLight(!this.getLight());
                        System.out.println("Light switched " + (this.getLight() ? "on" : "off") + ".");
                    }
                    default -> {
                        if (n == 4) {
                            if (this instanceof Kitchen kitchen) {
                                kitchen.setStove(!kitchen.getStove());
                                System.out.println("Stove switched " + (this.getLight() ? "on" : "off") + ".");
                                break;
                            }
                            if (this instanceof Bathroom bathroom) {
                                bathroom.setShower(!bathroom.getShower());
                                System.out.println("Shower switched " + (this.getLight() ? "on" : "off") + ".");
                                break;
                            }
                        }
                        System.out.println("Out of range!");
                    }
                }
            } else {
                scanner.next();
                System.out.println("Not an integer!");
            }
        }
        while (!leave);
    }
}
