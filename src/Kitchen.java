import java.util.List;

public class Kitchen extends Room {
    private boolean stove;

    public Kitchen(List<Room> neighbours) {
        super("Kitchen", neighbours);
        stove = false;
    }
    public boolean getStove() {
        return stove;
    }
    public void setStove(boolean stove) {
        this.stove = stove;
    }
}
